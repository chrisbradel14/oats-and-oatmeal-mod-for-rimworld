# Oats and Oatmeal Mod for RimWorld

An updated version of this mod: https://steamcommunity.com/sharedfiles/filedetails/?id=1262252469 for RimWorld version 1.0+.

Now available on the Steam workshop here: https://steamcommunity.com/sharedfiles/filedetails/?id=1803357494